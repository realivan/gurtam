from django.shortcuts import render
import requests
import json

headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
url = 'https://hst-api.wialon.com/wialon/ajax.html'


class Search(object):
    @staticmethod
    def start(request):
        # 1. Авторизироваться по токену:
        params = {
            'svc': 'token/login',
            'params': json.dumps({
                'token': 'c9e17dbe988bc1b97c6a6b0d1302de7a55FFF48C734CC0F5382808D320339D7C0F3151F9',
                'operateAs': 'demo_test'
            })
        }
        data = requests.post(url, data=params, headers=headers)
        params = data.json()
        id_session = params['eid']
        return render(request, 'index.html', {'data': id_session, 'start': 1})

    @staticmethod
    def obj(request):
        # 2. Получить список объектов:
        params = {
            'svc': 'core/search_items',
            'params': json.dumps({
                'spec': {
                    'itemsType': 'avl_unit',
                    'propName': 'sys_id',
                    'propValueMask': '*',
                    'sortType': 'sys_id'
                },
                'force': 1,
                'flags': 1,
                'from': 0,
                'to': 0
            }),
            'sid': request.COOKIES.get('session')
        }
        data = requests.post(url, params=params, headers=headers)
        try:
            data = data.json()['items']
            print('2-> ', data)
        except: return render(request, 'index.html', {'error': 'нажмите кнопку "Назад"'})
        # 3. Загрузить сообщения по объектам в сессию:
        messages = []
        for key in data:
            params = {
                'svc': 'messages/load_interval',
                'params': json.dumps({
                    'itemId': int(key['id']),
                    'timeFrom': int(request.COOKIES.get('from')[:10]),
                    'timeTo': int(request.COOKIES.get('to')[:10]),
                    'flags': 0x0001,
                    'flagsMask': 0xFF01,
                    'loadCount': 1
                }),
                'sid': request.COOKIES.get('session')
            }
            data1 = requests.post(url, params=params, headers=headers)
            data1 = data1.json()
            print('3-> ', data1)
            # 4. Получить сообщения:
            k = 0
            while k < int(data1['count']):
                params = {
                    'svc': 'messages/get_messages',
                    'params': json.dumps({
                        'indexFrom': k,
                        'indexTo': k+1,
                        'filter': 'pos.x,pos.y',
                    }),
                    'sid': request.COOKIES.get('session')
                }
                data2 = requests.post(url, params=params, headers=headers)
                params = data2.json()
                # определяем нахождение объекта в указанном радиусе
                if Search.radius(request, params[0]['pos']['y'], params[0]['pos']['x']) <= int(request.COOKIES.get('radius')):
                    messages.append(key['nm'])
                    print('4-> ', key['nm'])
                    break
                else:
                    # сделал выход из цикла, потому, что очень много сообщений от объекта от 26437 до 157225,
                    # и его все координаты приблизительно в одном месте.
                    # На мой взгляд достаточно анализа одного кортежа данных
                    # для определения нахождения в указанном радиусе,
                    # иначе обработка данных занимет много времени
                    break
                # k += 1 тк он недостигаемый из-за break

            # очистить загрузчик сообщений:
            params = {
                'svc': 'messages/unload',
                'params': json.dumps({}),
                'sid': request.COOKIES.get('session')
            }
            requests.post(url, params=params, headers=headers)

        return render(request, 'index.html', {'data': messages, 'len': len(messages)})

    @staticmethod
    def radius(request, x, y):
        import math
        # http://gis-lab.info/qa/great-circles.html
        # pi - число pi, rad - радиус сферы (Земли)
        rad = 6372795

        # координаты двух точек
        llat1 = float(request.COOKIES.get('lat'))
        llong1 = float(request.COOKIES.get('lng'))

        llat2 = float(x)
        llong2 = float(y)

        # в радианах
        lat1 = llat1*math.pi/180.
        lat2 = llat2*math.pi/180.
        long1 = llong1*math.pi/180.
        long2 = llong2*math.pi/180.

        # косинусы и синусы широт и разницы долгот
        cl1 = math.cos(lat1)
        cl2 = math.cos(lat2)
        sl1 = math.sin(lat1)
        sl2 = math.sin(lat2)
        delta = long2 - long1
        cdelta = math.cos(delta)
        sdelta = math.sin(delta)

        # вычисления длины большого круга
        y = math.sqrt(math.pow(cl2*sdelta, 2)+math.pow(cl1*sl2-sl1*cl2*cdelta, 2))
        x = sl1*sl2+cl1*cl2*cdelta
        ad = math.atan2(y, x)
        dist = ad*rad

        print('Расстояние = %.0f' % dist, ' [метров]')
        return dist
