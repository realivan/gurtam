function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getCookie(name) {
    var matches = document.cookie.split('; ');
    for(var i=0; i<matches.length; i++){
        var single = matches[i].split('=');
        if(single[0] == name){
            return single[1];
        }
    }
    return undefined;
}

function setMap(x, y) {
    map.setView([x, y], 3);
    setCookie('lat',x);
    setCookie('lng',y);
}

function setRadius(x){
    setCookie('radius',x);
    circle.setRadius(x);
}

function setTime(name, x){
    var t = x.split('/');
    t = Date.UTC(t[2], t[0], t[1]);
    setCookie(name, t);
}

$( '#search').click(function() {
    window.location.href = '/obj/';
});

$(function() {
    $( '.datepicker' ).datepicker({
        changeMonth: true,
        changeYear: true
    });
});

setCookie('lat', 0);
setCookie('lng', 0);
setCookie('radius', 100);

var map = L.map('map', {
    center: [0, 0],
    zoom: 3
});

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var circle = L.circle([0, 0], getCookie('radius')).addTo(map);

var areaSelect = L.areaSelect({width:150, height:150});
areaSelect.on("change", function() {
    var center = this.getBounds();
    $("#result .lon").val(center.getCenter().lng);
    $("#result .lat").val(center.getCenter().lat);
    $("#result .radius").val(getCookie('radius'));
    setCookie('lng', center.getCenter().lng);
    setCookie('lat', center.getCenter().lat);
    circle.setLatLng([center.getCenter().lat, center.getCenter().lng]);
});
areaSelect.addTo(map);