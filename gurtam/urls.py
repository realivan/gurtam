from django.conf.urls import include, url
from main.views import Search

urlpatterns = [
    url(r'^$', Search.start),
    url(r'^obj/$', Search.obj),
]
